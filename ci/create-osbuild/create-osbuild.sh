#!/bin/bash

set -euxo pipefail
# Get OS data.
source /etc/os-release
# the yum repo path
BUCKET=${BUCKET:-}
REGION=${AWS_REGION:-}
REPO_DIRNAME="${OS_PREFIX}${OS_VERSION}"
REPO_URL="${PIPELINE_REPOS_ENDPOINT}/${UUID}/${REPO_DIRNAME}"
LOCAL_REPO_DIR=${REPO_DIR:-"/var/lib/repos"}/${REPO_DIRNAME}
# DISK_IMAGE is the target for the Makefile and the name of the resulted file.
# Example: cs9-qemu-minimal-ostree.aarch64.img
DISK_IMAGE="${OS_PREFIX}${OS_VERSION}-${TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.${FORMAT}"
# .raw file is needed for import as AMI in S3
IMAGE_FILE=${IMAGE_FILE:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.raw"}

cd osbuild-manifests/osbuild-manifests/
if [[ "${UUID}" != "local" ]]; then
  # install osbuild and osbuild-tools, which contains osbuild-mpp utility
  dnf -y copr enable @osbuild/osbuild
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/@osbuild/osbuild/centos-stream-${OS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr\:copr.fedorainfracloud.org\:group_osbuild\:osbuild.repo

  dnf -y install osbuild osbuild-tools osbuild-ostree make

  # enable neptune copr repo
  dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${OS_VERSION}-\$basearch/"
  sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  # Set the pipeline's yum-repo
  echo "[+] Using the pipeline's yum-repo:"
  echo "repo: ${REPO_URL}"

  # set the OS_OPTIONS if this is a CI build, not a deployment
  if [[ ${S3_BUCKET_NAME} != *"downstream"* || ${PIPELINE_TASK_NAME} != *"product-build"* ]]; then
    OS_OPTIONS+=(ssh_permit_root_login="true")
    OS_OPTIONS+=(display_server=\"xorg\")
    OS_OPTIONS+=(cs${OS_VERSION}_baseurl=\"${REPO_URL}\")
    OS_OPTIONS+=(extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"])
    MPP_ARGS="-D 'root_ssh_key=\"${TF_SSH_KEY}\"'"
  else
    # do not add extra packages to sample image for product build
    MPP_ARGS=""		# do not add ssh key to image
  fi
  make ${DISK_IMAGE} \
       DEFINES="${OS_OPTIONS[*]}" \
       MPP_ARGS="${MPP_ARGS}"

else
  REVISION="main"

  # enable neptune copr repo
  sudo dnf -y copr enable pingou/qtappmanager-fedora
  SEARCH_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/epel-${OS_VERSION}-\$basearch/"
  REPLACE_PATTERN="baseurl=https://download.copr.fedorainfracloud.org/results/pingou/qtappmanager-fedora/centos-stream-${OS_VERSION}-\$basearch/"
  sudo sed -i -e "s|$SEARCH_PATTERN|$REPLACE_PATTERN|" \
    /etc/yum.repos.d/_copr:copr.fedorainfracloud.org:pingou:qtappmanager-fedora.repo

  make ${DISK_IMAGE} DEFINES="cs${OS_VERSION}_baseurl=\"${LOCAL_REPO_DIR}\" ${OS_OPTIONS[*]}"
fi

echo "[+] Moving the generated image"
sudo mkdir -p $(dirname $IMAGE_FILE)
sudo mv $DISK_IMAGE $IMAGE_FILE

# record some details of the input manifest, and disk image in json
cat <<EOF | sudo tee -a ${IMAGE_FILE%.*}.json
{
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "os_version": "${OS_PREFIX}${OS_VERSION}",
  "build_type": "${BUILD_TYPE}",
  "image_name": "${IMAGE_NAME}",
  "image_type": "${IMAGE_TYPE}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF

# Clean up
echo "[+] Cleaning up"
sudo rm -fr image_output osbuild_store

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
echo
