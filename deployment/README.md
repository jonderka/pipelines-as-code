# Deployment

The **A-Team pipeline** uses **OpenShift Pipelines** ([Tekton]) for building
and testing the different images.

The pipeline is defined as a set of tasks linked together in a specific
order. Some tasks will run in sequence, some in parallel.

For the integration with [Gitlab], the project uses a [webhook] which will
trigger the pipeline each time a developer files a MR (Merge Request), in
addition it can also be triggered using Tekton CLI ([tkn]).

Currently MR (Merge Request) from a source git fork are not supported.
MR from a source git branch are supported.

Merge request is also proposed by [rome](https://gitlab.com/redhat/edge/ci-cd/a-team/rome) via [dover](https://gitlab.com/redhat/edge/ci-cd/a-team/dover) to sync with the package updates from the upstream *cs9 (CentOS stream 9)*.

## Manifest-tools containerfile
[![Docker Repository on Quay](https://quay.io/repository/automotive-toolchain/manifest-tools/status "Docker Repository on Quay")](https://quay.io/repository/automotive-toolchain/manifest-tools)

## Installation

- Install the required modules listed in the `deployment/requirements.txt`
- Download and install [OpenShift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html)

## Prerequisite

- Login to the cluster
  - Using the `oc login` command retrieved from the openshift cluster

## Deploy the pipeline

The pipelines are deployed using [Ansible] and all the necessary files are in
this (`deployment`) directory. It's recommended to use the automated process.

### Spin stage environment [automated process]

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on any branch, then start `deploy_stage`.

### Spin prod environment [automated process]

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on the `main` branch, then start `deploy_prod`.

### Prerequisites [manual process]

It can be deployed by setting the appropiated credentials at the file
[vars/prod.yml](vars/prod.yml) or [vars/stage.yml](vars/stage.yml),
depending on the environment you like to deploy to.
Then, the deployment is done with `ansible`:

```shell
ansible-playbook main.yml -e env="$ENVIRONMENT"
```

Where `$ENVIRONMENT` is either `prod` or `stage`.

To simplify the workflow and make it more portable, there is a `Makefile` for
running it in a container with all the necessary dependencies. You just need
installed `podman` or `docker`, and `make`.

### Spin stage environment [manual process]

Run the following command from inside this directory:

```shell
make stage/up
```

### Spin prod environment [manual process]

Run the following command from inside this directory:

```shell
make prod/up
```

## Add new tasks to the pipeline

The pipeline tasks are defined at the file: [tasks/tasks.yml](tasks/tasks.yml).
Any change to the current tasks or the addition to new task should be made
there.

To understand better how the **tasks** work, it is recomended to look at the
upstream documentation for the [Tekton Tasks].

Then the task should be added to the pipeline at this file:
[tasks/pipeline.yml](tasks/pipeline.yml). Here is the upstream documentation
for the [Tekton Pipelines].

One thing to notice is that the format might be a little different from the
examples on that documentation. That's because this project uses Ansible
for generating all those resources.

[Tekton]: https://tekton.dev/
[Gitlab]: https://gitlab.com
[webhook]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[tkn]: https://tekton.dev/docs/cli/
[Ansible]: https://docs.ansible.com/
[Tekton Tasks]: https://github.com/tektoncd/pipeline/blob/main/docs/tasks.md
[Tekton Pipelines]: https://github.com/tektoncd/pipeline/blob/main/docs/pipelines.md

## Product Build pipeline deployment and usage

### Product Build Prerequisites [manual process]

It can be deployed by setting the appropiated credentials at the file
[vars/prod.yml](vars/prod.yml) or [vars/stage.yml](vars/stage.yml),
depending on the environment you like to deploy to.
Then, the deployment is done with `ansible`:

```shell
ansible-playbook publish-product-build.yml -e env="$ENVIRONMENT"
```

Where `$ENVIRONMENT` is either `prod` or `stage`.

To simplify the workflow and make it more portable, there is a `Makefile` for
running it in a container with all the necessary dependencies. You just need
installed `podman` or `docker`, and `make`.

#### Webhook Deployment

- Create or edit an existing webhook on a Git repository

    example URL: `http://a-team-<namespace>.apps.<cluster-name>.openshiftapps.com`

    *note: the `a-team` prefix is the name of the event listener route, which may differ depending on your deployment and setup*

- Add `push` events to the trigger rules referring to your main branch

### Spin stage environment for product-build [automated process]

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on any branch, then start `deploy_stage_product_build`.

### Spin prod environment for product-build [automated process]

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on the `main` branch, then start `deploy_prod_product_build`.

### Downloading product-build

To download product-build run the following command:

***pre-req: valid AWS credentials in the user environment:***

```sh
    export AWS_ACCESS_KEY_ID=<your-aws-access-key-id>
    export AWS_SECRET_ACCESS_KEY=<your-aws-secret-access-key>
```

```sh
    aws s3 cp s3://auto-product-build/product-build-<timestamp>/cs9 <your-desired-folder> --recursive
```

Or

***Note: 'latest' prefix tends to be unavailable at some point during the build time***
```sh
    aws s3 cp s3://auto-product-build/latest/cs9 <your-desired-folder> --recursive
```
